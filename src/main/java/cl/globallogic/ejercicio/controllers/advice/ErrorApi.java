package cl.globallogic.ejercicio.controllers.advice;

public class ErrorApi {

	private String mensaje;

	public ErrorApi(String mensaje) {
		super();
		this.mensaje = mensaje;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
}

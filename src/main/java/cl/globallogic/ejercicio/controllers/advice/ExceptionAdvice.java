package cl.globallogic.ejercicio.controllers.advice;

import java.security.AccessControlException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.context.request.WebRequest;

import io.jsonwebtoken.JwtException;

@RestControllerAdvice
public class ExceptionAdvice {

	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<List<ErrorApi>> manejarExcepcion(MethodArgumentNotValidException bindingException) {
		BindingResult result = bindingException.getBindingResult();
        List<FieldError> fieldErrors = result.getFieldErrors();
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(processFieldErrors(fieldErrors));
	}
	private List<ErrorApi> processFieldErrors(List<FieldError> fieldErrors) {
        List<ErrorApi> errores = new ArrayList<ErrorApi>();
        for (FieldError fieldError: fieldErrors) {
        	errores.add(new ErrorApi("Error de validacion: " + fieldError.getDefaultMessage()));
        }
        return errores;
    }

	@ExceptionHandler(JwtException.class)
	public ResponseEntity<ErrorApi> manejarExcepcionJwt(JwtException jwtException) {
		ErrorApi error = new ErrorApi(jwtException.getMessage());	
		return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(error);
	}
	
	@ExceptionHandler({AccessControlException.class})
	public ResponseEntity<ErrorApi> manejarExcepcionControlAcceso(AccessControlException accessControlException) {
		ErrorApi error = new ErrorApi(accessControlException.getMessage());	
		return ResponseEntity.status(HttpStatus.FORBIDDEN).body(error);
	}

	@ExceptionHandler({ HttpServerErrorException.class })
	public ResponseEntity<?> handle500Series(Exception ex, WebRequest request) {
		ErrorApi error = new ErrorApi("Error de API: Ha Ocurrido un Error Interno del Servidor.");
		return new ResponseEntity<Object>(error, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler({ HttpClientErrorException.class })
	public ResponseEntity<?> handle400Series(Exception ex, WebRequest request) {
		ErrorApi error = new ErrorApi("Error de API: Ha Ocurrido un error.");
		return new ResponseEntity<Object>(error, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
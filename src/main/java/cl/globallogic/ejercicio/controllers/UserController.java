package cl.globallogic.ejercicio.controllers;

import java.security.AccessControlException;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import cl.globallogic.ejercicio.controllers.advice.ErrorApi;
import cl.globallogic.ejercicio.forms.LoginForm;
import cl.globallogic.ejercicio.forms.UserForm;
import cl.globallogic.ejercicio.models.User;
import cl.globallogic.ejercicio.services.JwtService;
import cl.globallogic.ejercicio.services.UserService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class UserController {

	@Autowired
	private UserService userService;
	
	@Autowired
	private JwtService jwtService;
	
	@ApiOperation(value = "Crea un nuevo usuario")
	@ApiResponses({
			@ApiResponse(code = 201, message = "Usuario creado exitosamente", response = User.class),
			@ApiResponse(code = 409, message = "Problema al registrar el usuario", response = ErrorApi.class), })
	@PostMapping(value = "/v1/users", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> insertaUser(
			@RequestHeader(required = true) String headerAuthorization,
			@Valid @RequestBody UserForm userForm, BindingResult bindingResult) {
		if(bindingResult.hasErrors()) {
			List<ErrorApi> errores = new ArrayList<ErrorApi>();
	        for (FieldError fieldError: bindingResult.getFieldErrors()) {
	        	errores.add(new ErrorApi("Error de validacion: " + fieldError.getDefaultMessage()));
	        }
	        return ResponseEntity.status(HttpStatus.CONFLICT).body(errores);
		}
		User user = userService.save(userForm);
		if (null != user) {
			return ResponseEntity.status(HttpStatus.CREATED).body(user);
		} else {
			ErrorApi error = new ErrorApi("Ha ocurrido un error al intentar registrar al usuario");
			return ResponseEntity.status(HttpStatus.CONFLICT).body(error);
		}
	}
	
	
	@ApiOperation(value = "Obtiene un usuario por id")
	@ApiResponses({ @ApiResponse(code = 404, message = "Usuario no encontrado", response = ErrorApi.class),
			@ApiResponse(code = 200, message = "Usuario encontrado", response = User.class) })
	@GetMapping(value = "/v1/users/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> obtieneUser(
			@RequestHeader(required = true) String headerAuthorization,
			@ApiParam(name = "id", value = "Id del usuario", required = true) @PathVariable("id") String id) {
		
		String emailToken = obtieneEmailToken(headerAuthorization);
		User user = userService.findById(id);
		if (null != user) {
			validarAutorizacion(emailToken, user.getEmail());
			return ResponseEntity.status(HttpStatus.OK).body(user);
		} else {
			ErrorApi error = new ErrorApi("Usuario no encontrado");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(error);
		}
	}
	
	@ApiOperation(value = "Login")
	@ApiResponses({ 
			@ApiResponse(code = 200, message = "Usuario encontrado", response = User.class),
			@ApiResponse(code = 403, message = "Prohibido", response = ErrorApi.class)
			})
	@PostMapping(value = "/v1/users/login", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> login(@RequestBody LoginForm loginForm) {
		User user = userService.login(loginForm);
		if (null != user) {
			return ResponseEntity.status(HttpStatus.OK).body(user);
		} else {
			ErrorApi error = new ErrorApi("Usuario o clave incorrectos");
			return ResponseEntity.status(HttpStatus.FORBIDDEN).body(error);
		}
	}
	
	private String obtieneEmailToken(String headerAuthorization) {
		if (jwtService.validaToken(headerAuthorization)) {
			String emailToken  = (String) jwtService.getClaim(headerAuthorization, "email");
			return emailToken;
		}
		return null;
	}

	private void validarAutorizacion(String emailToken ,String emailUser) {
		if (!emailUser.equals(emailToken) && !emailToken.equals("admin@admin.cl")) {
			throw new AccessControlException("No tiene permisos para la consulta");
		}
	}

}

package cl.globallogic.ejercicio.repositories;


import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cl.globallogic.ejercicio.models.User;

@Repository
public interface UserRespository extends JpaRepository<User, String>{

	Optional<User> findByEmail(String email);
}

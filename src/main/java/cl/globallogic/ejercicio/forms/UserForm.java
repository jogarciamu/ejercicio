package cl.globallogic.ejercicio.forms;

import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import cl.globallogic.ejercicio.validators.UserFormValidator;

@UserFormValidator
public class UserForm {

	@NotBlank(message = "El campo name no puede ser vacio.")
	private String name;
	
	@NotBlank(message = "El campo mail no puede ser vacio")
	@Email(message = "El campo email no tiene un formato de correo electronico válido.")
	private String email;
	
	@NotBlank
	@Pattern(regexp = "^(?:[a-z]*[A-Z][a-z]+|[a-z]+[A-Z][a-z]*)([0-9]{2})$", message = "El campo password de contener 1 mayúscula, minusculas y 2 números.")
	private String password;
	private List<PhoneForm> phones;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public List<PhoneForm> getPhones() {
		return phones;
	}
	public void setPhones(List<PhoneForm> phones) {
		this.phones = phones;
	}
	
	
}

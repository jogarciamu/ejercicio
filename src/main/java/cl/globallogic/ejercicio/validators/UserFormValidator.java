package cl.globallogic.ejercicio.validators;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.security.AccessControlException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import cl.globallogic.ejercicio.forms.UserForm;
import cl.globallogic.ejercicio.models.User;
import cl.globallogic.ejercicio.services.JwtService;
import cl.globallogic.ejercicio.services.UserService;
import cl.globallogic.ejercicio.validators.UserFormValidator.UserFormValidatorHelper;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Constraint(validatedBy = UserFormValidatorHelper.class)
public @interface UserFormValidator {
	String message() default "No es posible agregar el user";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

	public class UserFormValidatorHelper implements ConstraintValidator<UserFormValidator, UserForm> {

		@SuppressWarnings("unused")
		private UserFormValidator anotacion;

		@Autowired
		private HttpServletRequest request;
		
		@Autowired
		private UserService userService;
		
		@Autowired
		private JwtService jwtService;

		@Override
		public void initialize(UserFormValidator anotacion) {
			this.anotacion = anotacion;
		}

		@Override
		public boolean isValid(UserForm userForm, ConstraintValidatorContext ctx) {
			String authorization =  request.getHeader("headerAuthorization");
			if(StringUtils.isEmpty(authorization)) {
				ctx.disableDefaultConstraintViolation();
				ctx.buildConstraintViolationWithTemplate("Debe enviar el token")
						.addPropertyNode("headerAuthorization").addConstraintViolation();
				return false;
			}
			String emailToken = obtieneEmailToken(authorization);
			validarAutorizacion(emailToken);
			
			User user = userService.findByEmail(userForm.getEmail());
			if (null!=user) {
				ctx.disableDefaultConstraintViolation();
				ctx.buildConstraintViolationWithTemplate("El email que intenta registrar ya se encuentra almacenado")
						.addPropertyNode("email").addConstraintViolation();
				return false;
			}	
			return true;
		}
		
		
		private String obtieneEmailToken(String headerAuthorization) {
			if (jwtService.validaToken(headerAuthorization)) {
				String emailToken  = (String) jwtService.getClaim(headerAuthorization, "email");
				return emailToken;
			}
			return null;
		}

		private void validarAutorizacion(String emailToken) {
			if (!emailToken.equals("admin@admin.cl")) {
				throw new AccessControlException("No tiene permisos para la consulta");
			}
		}
	}
}
